var _todoArray = [];
var _topPointer = -1;

$(document).ready(function () {
  bindAllEvents();
});

function addToDoInArray(todo) {
  if (!todo) {
    return;
  }
  _todoArray.push(todo);
  _topPointer++;
  //   storeKeyValuePair('todo', [..._todoArray]);
}

function unmountList() {
  $('#todo_list_display').empty();
}

function deleteTodo(index) {
  _todoArray.splice(index, 1);
  _topPointer--;
  unmountList();
  reRenderList();
}

function editTodoSetDisplay(index) {
  $(`#value_${index}`).addClass('sec-hide');
  $(`#edit_input_${index}`).removeClass('sec-hide');
  $(`#edit_input_${index}`).val(_todoArray[index]);
  $(`#edit_delete_cont_${index}`).addClass('sec-hide');
  $(`#save_edit_btn_${index}`).removeClass('sec-hide');
}

function onClickSaveEditTodo (index) {
  _todoArray[index] = $(`#edit_input_${index}`).val();
  // storeKeyValuePair('todo', [..._todoArray]);
  unmountList();
  reRenderList();
};

function onClickAddHandler() {
  addToDoInArray($('#todo_input').val());
  $('#todo_input').val('');
  unmountList();
  reRenderList();
}

function reRenderList() {
  _todoArray.forEach((todo, index) => {
    $('#todo_list_display').append(`
        <div 'id=todo_${index}' class='todo-item'>

            <div id='value_${index}'>
                ${todo}
            </div>

            <input id= 'edit_input_${index}' class='todo-input-small sec-hide' />

            <div id='edit_delete_cont_${index}' >
                <button class='add-button B1' id='edit_btn${index}'>e</button>
                <button class='add-button B1' id='delete_btn${index}'>x</button>
            </div>

            <button class='add-button B1 sec-hide' id='save_edit_btn_${index}' >s</button>

        </div>
        `);
    $(`#delete_btn${index}`).on('click', function () {
      deleteTodo(index);
    });
    $(`#edit_btn${index}`).on('click', function () {
      editTodoSetDisplay(index);
    });
    $(`#save_edit_btn_${index}`).on('click', function(){
      onClickSaveEditTodo(index);
    });
    $(`#edit_input_${index}`).on('keydown', function (event){ 
      if(event.keyCode === 13){
        onClickSaveEditTodo(index);
      }
    })
  });
}

/**
 * This function binds all event handles at the time of page load.
 */
function bindAllEvents() {
  $('#add_todo_btn').on('click', onClickAddHandler);
  $('#main-header').on('focus', function () {
    $('#main-header').addClass('main-header-focus');
  });
  $('#main-header').off('focus', function () {
    $('#main-header').addClass('main-header');
  });
  $('#todo_input').on('keydown',function(event){
    if(event.keyCode == 13) {
       onClickAddHandler();
    }
  });

}

// function storeKeyValuePair(key, value) {
//     if (typeof(Storage) !== 'undefined') {
//       localStorage.setItem(key, JSON.stringify(value));
//     } else {
//       console.log('Local storage is not supported in this browser.');
//     }
//   }
